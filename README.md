# My configurations files of my rice on linux

## dmenu - 5.0
	Dynamic menu from suckless project

	patches:
		numbers

## dwm - 6.2
	Contains the dwm tiling window manager source code.

	patches:
		cfacts
		fullgaps
		pertag
		status2D
		swallow

## st - 0.8.4
	simple terminal from suckless project.

	patches:
		alpha
		hidecursor
		scrollback
		w3m

## external links
	https://suckless.org
	https://git.suckless.org/<package-name>
