#!/usr/bin/env sh

####################################
# Void linux environment installer #
####################################

# install packages
xbps-install -Syu alsa-lib-devel gcc libXft-devel libXinerama-devel make pkgconf setxkbmap vim xmodmap xorg-minimal feh xcompmgr
# install suckless software
cd dwm
make install
cd ../st
make install
cd ../dmenu
make install
cd ..
# copy fonts
mkdir -p /usr/share/fonts/TTF
cp configs/fonts/* /usr/share/fonts/TTF
# copy dot files
cd configs/dotfiles
cp .Xmodmap .bashrc .vimrc .xinitrc ../../..
cd
