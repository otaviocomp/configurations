" basic setup vim
set tabstop=8
set shiftwidth=8
set number
set showmatch
set autoindent
set smartindent
set wrap linebreak
set incsearch
set shortmess-=S
set virtualedit=all
set showbreak=...
syntax on

" autocommands
au BufNewFile,BufRead *.ms   set textwidth=80
au BufNewFile,BufRead *.txt  set textwidth=100
au BufNewFile,BufRead *.tex  set textwidth=80
au BufNewFile,BufRead *.tex  set tabstop=2 
au BufNewFile,BufRead *.tex  set expandtab
au BufNewFile,BufRead *.tex  set shiftwidth=2

" custom maps
map ; :wa<CR>
map t :wa \| !make -j2<CR>
map T :wa \| !make all -j2<CR>
map z :center<CR>
map m o<ESC>k
