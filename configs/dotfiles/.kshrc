alias ls='colorls -G'

# Am I root?
if [ $(id -g) -eq 0 ]; then
	PS1="\033[31m[ \w ]$ \033[0m"
else
	PS1="\033[36m[ \w ]$ \033[0m"
fi
