/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 8;       /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
#if defined(__linux__)
static const char *fonts[]          = { "Hack Nerd Font:size=12:style=Bold" };
static const char dmenufont[]       = "Hack Nerd Font:size=12:style=Bold";
#elif defined(__FreeBSD__)
static const char *fonts[]          = { "Hack Nerd Font:size=8:style=Bold" };
static const char dmenufont[]       = "Hack Nerd Font:size=8:style=Bold";
#endif
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
//static const char col_gray3[]       = "#F9E5C6"; /* fg tags */
//static const char col_gray4[]       = "#FA955A"; /* sel tag */
static const char col_gray3[]       = "#A9A9F5"; /* fg tags */
static const char col_gray4[]       = "#81DAF5"; /* sel tag */
static const char custom1[]         = "#000000"; /* bg sel */
static const char custom2[]         = "#000000"; /* bg bar and tags */
static const char custom3[]         = "#aaaaaa"; /* sel border tile */
static const char custom4[]         = "#832323"; /* bg dmenu */
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, custom2, col_gray2 },
	[SchemeSel]  = { col_gray4, custom1,  custom3  },
};

/* tagging */
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class         instance  title          tags mask  isfloating  isterminal  noswallow  monitor */
	{ "st",          NULL,     NULL,          0,         0,          1,           0,        -1 },
	{ "st",          NULL,     "audiorec",    1 << 4,    0,          1,           0,        -1 },
	{ "st",          NULL,     "screenrec",   1 << 4,    0,          1,           0,        -1 },
	{ "st",          NULL,     "fullrec",     1 << 4,    0,          1,           0,        -1 },
	{ "krita",       NULL,     NULL,          1 << 7,    0,          0,           1,        -1 },
	{ "Firefox",     NULL,     NULL,          1 << 8,    0,          0,           1,        -1 },
	{ NULL,          NULL,    "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]     = { "dmenu_run", "-i", "-m", dmenumon, "-fn", dmenufont,
                                      "-nb", custom2, "-nf", col_gray3, "-sb", custom4,
                                      "-sf", col_gray3, NULL };
static const char *termcmd[]      = { "st", NULL };
#if defined(__linux__)
static const char *upvol[]        = { "amixer", "-q", "sset", "Master", "1%+", NULL };
static const char *upvol5[]       = { "amixer", "-q", "sset", "Master", "5%+", NULL };
static const char *upvol20[]      = { "amixer", "-q", "sset", "Master", "20%+", NULL };
static const char *downvol[]      = { "amixer", "-q", "sset", "Master", "1%-", NULL };
static const char *downvol5[]     = { "amixer", "-q", "sset", "Master", "5%-", NULL };
static const char *downvol20[]    = { "amixer", "-q", "sset", "Master", "20%-", NULL };
static const char *upmic[]        = { "amixer", "-q", "sset", "Capture", "1%+", NULL };
static const char *upmic5[]       = { "amixer", "-q", "sset", "Capture", "5%+", NULL };
static const char *upmic20[]      = { "amixer", "-q", "sset", "Capture", "20%+", NULL };
static const char *downmic[]      = { "amixer", "-q", "sset", "Capture", "1%-", NULL };
static const char *downmic5[]     = { "amixer", "-q", "sset", "Capture", "5%-", NULL };
static const char *downmic20[]    = { "amixer", "-q", "sset", "Capture", "20%-", NULL };
static const char *mutevol[]      = { "amixer", "-q", "sset", "Master", "toggle", NULL };
static const char *mutemic[]      = { "amixer", "sset", "'Capture',0", "toggle", NULL };
#elif defined(__FreeBSD__)
static const char *upvol[]        = { "mixer", "-s", "vol", "+1" , NULL };
static const char *upvol5[]       = { "mixer", "-s", "vol", "+5" , NULL };
static const char *upvol20[]      = { "mixer", "-s", "vol", "+20", NULL };
static const char *downvol[]      = { "mixer", "-s", "vol", "-1" , NULL };
static const char *downvol5[]     = { "mixer", "-s", "vol", "-5" , NULL };
static const char *downvol20[]    = { "mixer", "-s", "vol", "-20", NULL };
static const char *upmic[]        = { "mixer", "-s", "rec", "+1" , NULL };
static const char *upmic5[]       = { "mixer", "-s", "rec", "+5" , NULL };
static const char *upmic20[]      = { "mixer", "-s", "rec", "+20", NULL };
static const char *downmic[]      = { "mixer", "-s", "rec", "-1" , NULL };
static const char *downmic5[]     = { "mixer", "-s", "rec", "-5" , NULL };
static const char *downmic20[]    = { "mixer", "-s", "rec", "-20", NULL };
static const char *mutevol[]      = { "mixer", "-s", "vol", "0"  , NULL };
static const char *mutemic[]      = { "mixer", "-s", "rec", "0", NULL };
#endif
static const char *brightup[]     = { "xbacklight", "-inc", "1", NULL };
static const char *brightup5[]    = { "xbacklight", "-inc", "5", NULL };
static const char *brightup20[]   = { "xbacklight", "-inc", "20", NULL };
static const char *brightdown[]   = { "xbacklight", "-dec"," 1", NULL };
static const char *brightdown5[]  = { "xbacklight", "-dec"," 5", NULL };
static const char *brightdown20[] = { "xbacklight", "-dec"," 20", NULL };
static const char *slock[]        = { "slock", NULL };
static const char *refbar[]       = { "/home/ota/cstatusbar/refbar", NULL };
static const char *openfile[]     = { "/home/ota/configurations/scripts/openfile", NULL };
static const char *printscreen[]  = { "/home/ota/configurations/scripts/printscreen", NULL };
static const char *youtubehq[]    = { "/home/ota/configurations/scripts/ythq", NULL };
static const char *youtubelq[]    = { "/home/ota/configurations/scripts/ytlq", NULL };
static const char *keyboard[]     = { "/home/ota/configurations/scripts/keyboard", NULL };
static const char *audiorec[]     = { "st", "/home/ota/configurations/scripts/audiorec", NULL };
static const char *screenrec[]    = { "st", "/home/ota/configurations/scripts/screenrec", NULL };
static const char *fullrec[]      = { "st", "/home/ota/configurations/scripts/fullrec", NULL };
static const char *stoprecorder[] = { "killall", "ffmpeg", NULL };
static const char *browser[]      = { "firefox", NULL };
//static const char *telegram[]     = { "telegram-desktop", NULL };

static Key keys[] = {
	/* modifier               key        function        argument */
	{ MODKEY,                 XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                 XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                 XK_w,      togglebar,      {0} },
	{ MODKEY,                 XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                 XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                 XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                 XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY,                 XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                 XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,       XK_Return, zoom,           {0} },
	{ MODKEY,                 XK_Tab,    view,           {0} },
	{ MODKEY,                 XK_q,      killclient,     {0} },
	{ MODKEY,                 XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                 XK_g,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                 XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                 XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,       XK_space,  togglefloating, {0} },
	{ MODKEY,                 XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,       XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                 XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                 XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,       XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,       XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                 XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                 XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,       XK_equal,  setgaps,        {.i = 0  } },
	{ MODKEY|ShiftMask,       XK_h,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,       XK_l,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,       XK_o,      setcfact,       {.f =  0.00} },
	TAGKEYS(                  XK_1,                      0)
	TAGKEYS(                  XK_2,                      1)
	TAGKEYS(                  XK_3,                      2)
	TAGKEYS(                  XK_4,                      3)
	TAGKEYS(                  XK_5,                      4)
	TAGKEYS(                  XK_6,                      5)
	TAGKEYS(                  XK_7,                      6)
	TAGKEYS(                  XK_8,                      7)
	TAGKEYS(                  XK_9,                      8)
	{ MODKEY|ShiftMask,       XK_q,      quit,           {0} },
	{ MODKEY|ShiftMask,       XK_l,      spawn,          {.v = slock } },

	/********************************/
	/* custom functions and scripts */
	/********************************/

	/* audio keys */
	{ Mod4Mask|MODKEY,        XK_comma,     spawn, {.v = upmic } },
	{ Mod4Mask,               XK_comma,     spawn, {.v = upmic5 } },
	{ Mod4Mask|ShiftMask,     XK_comma,     spawn, {.v = upmic20 } },
	{ Mod4Mask|MODKEY,        XK_m,         spawn, {.v = downmic } },
	{ Mod4Mask,               XK_m,         spawn, {.v = downmic5 } },
	{ Mod4Mask|ShiftMask,     XK_m,         spawn, {.v = downmic20 } },
	{ Mod4Mask|MODKEY,        XK_slash,     spawn, {.v = upvol } },
	{ Mod4Mask,               XK_slash,     spawn, {.v = upvol5 } },
	{ Mod4Mask|ShiftMask,     XK_slash,     spawn, {.v = upvol20 } },
	{ Mod4Mask|MODKEY,        XK_period,    spawn, {.v = downvol } },
	{ Mod4Mask,               XK_period,    spawn, {.v = downvol5 } },
	{ Mod4Mask|ShiftMask,     XK_period,    spawn, {.v = downvol20 } },
	{ Mod4Mask,               XK_n,         spawn, {.v = mutevol } },
	{ Mod4Mask|ShiftMask,     XK_n,         spawn, {.v = mutemic } },

	/* brightness keys */
	//{ Mod4Mask|MODKEY,        XK_slash,     spawn, {.v = brightup } },
	//{ Mod4Mask,               XK_slash,     spawn, {.v = brightup5 } },
	//{ Mod4Mask|ShiftMask,     XK_slash,     spawn, {.v = brightup20 } },
	//{ Mod4Mask|MODKEY,        XK_period,    spawn, {.v = brightdown } },
	//{ Mod4Mask,               XK_period,    spawn, {.v = brightdown5 } },
	//{ Mod4Mask|ShiftMask,     XK_period,    spawn, {.v = brightdown20 } },

	/* refbar - refresh status bar */
	{ Mod4Mask,               XK_comma,     spawn, {.v = refbar } },
	{ Mod4Mask|ShiftMask,     XK_comma,     spawn, {.v = refbar } },
	{ Mod4Mask|MODKEY,        XK_comma,     spawn, {.v = refbar } },
	{ Mod4Mask,               XK_m,         spawn, {.v = refbar } },
	{ Mod4Mask|ShiftMask,     XK_m,         spawn, {.v = refbar } },
	{ Mod4Mask|MODKEY,        XK_m,         spawn, {.v = refbar } },
	{ Mod4Mask,               XK_n,         spawn, {.v = refbar } },
	{ Mod4Mask|ShiftMask,     XK_n,         spawn, {.v = refbar } },
	{ Mod4Mask,               XK_slash,     spawn, {.v = refbar } },
	{ Mod4Mask|ShiftMask,     XK_slash,     spawn, {.v = refbar } },
	{ Mod4Mask|MODKEY,        XK_slash,     spawn, {.v = refbar } },
	{ Mod4Mask,               XK_period,    spawn, {.v = refbar } },
	{ Mod4Mask|ShiftMask,     XK_period,    spawn, {.v = refbar } },
	{ Mod4Mask|MODKEY,        XK_period,    spawn, {.v = refbar } },

	/* launch programs */
	{ Mod4Mask,               XK_o,         spawn, {.v = openfile } },
	{ Mod4Mask,               XK_p,         spawn, {.v = printscreen } },
	{ Mod4Mask,               XK_i,         spawn, {.v = browser } },
	{ Mod4Mask,               XK_a,         spawn, {.v = audiorec} },
	{ Mod4Mask,               XK_s,         spawn, {.v = screenrec } },
	{ Mod4Mask,               XK_d,         spawn, {.v = fullrec } },
	{ Mod4Mask,               XK_x,         spawn, {.v = stoprecorder } },
	{ Mod4Mask,               XK_y,         spawn, {.v = youtubehq } },
	{ Mod4Mask,               XK_u,         spawn, {.v = youtubelq } },
	{ MODKEY|ShiftMask,       XK_k,         spawn, {.v = keyboard} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

